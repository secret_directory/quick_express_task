const logs = {};

async function log(req, res, next){
    const id = Math.random().toString(20).substr(2,18);
    const entry = {
        method: req.method,
        url: req.url,
        headers: req.headers,
        query: req.query,
        body: req.body,
        timestamp: Date.now()
    };

    logs[id] = entry;
    next();
}

async function getLogs(req, res){
    res.json(logs);
}

async function deleteLogs(req,res) {
    Object.keys(logs).forEach(key => {
        delete logs[key];
    });
    res.json({ message: 'Logs removed'})
}

module.exports = {
    log,
    getLogs,
    deleteLogs
};