const express = require("express");
const app = express();

let logs = [];

function addLog(level, message){
    const log = {
        timestamp: new Date().toISOString(),
        level,
        message
    };
    logs.push(log);
    console.log(log);
}

async function info(message){
    await new Promise(resolve => setTimeout(resolve, 0));
    addLog("info", message);
}

async function warn(message){
    await new Promise(resolve => setTimeout(resolve, 0));
    addLog("warn", message);
}

async function debug(message){
    await new Promise(resolve => setTimeout(resolve, 0));
    addLog("debug", message);
}

app.get("/logs", async (req, res) => {
    await new Promise(resolve => setTimeout(resolve, 0));
    const sortLogs = logs.slice().reverse();
    res.json(sortLogs);
});

app.delete("/logs", async(req, res) => {
    await new Promise(resolve => setTimeout(resolve, 0));
    logs = [];
    res.json({ message: 'Logs deleted'});
});

app.listen(3000, () => {
    console.log("Server listening on port 3000");
});

module.exports = {
    info, warn, debug
};