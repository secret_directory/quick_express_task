const express = require('express');
const { log, getLogs, deleteLogs } = require('./loggerModule');

const app = express();
const port = 3000;

app.use(log);

app.get('/sayHi', (req, res) => {
    res.json({ message: 'Hello ' + req.query.name});
});

app.get('/logs', getLogs);

app.delete('/logs', deleteLogs);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  });